﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Core.Commands
{
    public class Roles : ModuleBase<SocketCommandContext>
    {
        [Command("addroles"), Summary("Adds Roles")]

        public async Task AddRoles()
        {
            await Context.Guild.CreateRoleAsync("Iron", null, null, false, null);
            await Context.Guild.CreateRoleAsync("Bronze", null, null, false, null);
            await Context.Guild.CreateRoleAsync("Silver", null, null, false, null);
            await Context.Guild.CreateRoleAsync("Gold", null, null, false, null);

            await Context.Channel.SendMessageAsync("Half Done");

            await Context.Guild.CreateRoleAsync("Platinum", null, null, false, null);
            await Context.Guild.CreateRoleAsync("Diamond", null, null, false, null);
            await Context.Guild.CreateRoleAsync("Master", null, null, false, null);
            await Context.Guild.CreateRoleAsync("GrandMaster", null, null, false, null);
            await Context.Guild.CreateRoleAsync("Challenger", null, null, false, null);

            await Context.Channel.SendMessageAsync("Done");
        }

        [Command("refresh"), Summary("Refresh All Roles")]

        public async Task Refresh()
        {
            if(Context.User.Id != 136920995545088001)
            {
                return;
            }

            var users = Context.Guild.Users;
            var roles = Context.Guild.Roles;

            var RolesList = new List<SocketRole>();

            foreach (var role in roles)
            {
                RolesList.Add(role);
            }

            foreach (var user in users)
            {
                var time = (DateTime.Now - user.JoinedAt).Value.TotalDays;
                int days = Convert.ToInt32(time);

                foreach (var role in user.Roles)
                {
                    if (role.Name == "Iron" || role.Name == "Bronze" || role.Name == "Silver" || role.Name == "Gold" || role.Name == "Platinum" || role.Name == "Diamond" || role.Name == "Master" || role.Name == "GrandMaster" || role.Name == "Challenger")
                    {
                        await user.RemoveRoleAsync(role);
                    }
                }

                if (time < 7)
                {
                    var assignRole = RolesList.Find(x => x.Name == "Iron");
                    await user.AddRoleAsync(assignRole);
                    if(days == 0)
                    {
                        await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **today**");
                    }
                    else
                    {
                        await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                    }

                }
                else if (time < 30)
                {
                    var assignRole = RolesList.Find(x => x.Name == "Bronze");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }
                else if (time < 91)
                {
                    var assignRole = RolesList.Find(x => x.Name == "Silver");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }
                else if (time < 182)
                {
                    var assignRole = RolesList.Find(x => x.Name == "Gold");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }
                else if (time < 365)
                {
                    var assignRole = RolesList.Find(x => x.Name == "Platinum");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }
                else if (time < 730)
                {
                    var assignRole = RolesList.Find(x => x.Name == "Diamond");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }
                else if (time < 1826)
                {
                    var assignRole = RolesList.Find(x => x.Name == "Master");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }
                else if (time < 3652)
                {
                    var assignRole = RolesList.Find(x => x.Name == "GrandMaster");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }
                else
                {
                    var assignRole = RolesList.Find(x => x.Name == "Challenger");
                    await user.AddRoleAsync(assignRole);
                    await Context.Channel.SendMessageAsync($"The user **{user.Mention}** was assigned the role **{assignRole.ToString()}** because they joined the server **{days.ToString()} days ago**");
                }

            }
        }

        [Command("myrole"), Summary("Show user's role/info")]

        public async Task MyRole(/*IUser User = null*/)
        {
            SocketGuildUser User = Context.User as SocketGuildUser;

            if (User != null )
            {
                var time = (DateTime.Now -User.JoinedAt).Value.TotalDays;
                int days = Convert.ToInt32(time);

                foreach (var role in User.Roles)
                {
                    if (role.Name == "Iron" || role.Name == "Bronze" || role.Name == "Silver" || role.Name == "Gold" || role.Name == "Platinum" || role.Name == "Diamond" || role.Name == "Master" || role.Name == "GrandMaster" || role.Name == "Challenger")
                    { 
                        await Context.Channel.SendMessageAsync($":cool: {User.Mention} is {role.Name} (Joined {days} ago)");
                    }
                }

                
            }
        }
    }
}
