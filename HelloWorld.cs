﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace DiscordBot.Core.Commands
{
    public class HelloWorld : ModuleBase<SocketCommandContext>
    {
        [Command("hello"), Alias("Helloworld", "world"), Summary("Hello World Command")]

        public async Task Hello()
        {
            //if (Context.User.Id != 136920995545088001)
            //{
            //    return;
            //}

            await Context.Channel.SendMessageAsync($"Hello {Context.User.Username.ToString()}");
        }

        [Command("embed"), Summary("Embed Text Command")]

        public async Task Embed([Remainder] string Input = "None")
        {
            EmbedBuilder Embed = new EmbedBuilder();
            Embed.WithAuthor("Test Embed", Context.User.GetAvatarUrl());
            Embed.WithColor(0, 191, 255);
            Embed.WithFooter("Embed Footer", Context.Guild.Owner.GetAvatarUrl());
            Embed.WithDescription("Yada **yada** yada. \n" + "[__Google__](https://google.com/)");

            await Context.Channel.SendMessageAsync("", false, Embed.Build());
        }
    }
}
